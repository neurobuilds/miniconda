ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG PYTHON_VERSION=3.10.4


FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-base
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           wget \
           bzip2 && \
    rm -rf /var/lib/apt/lists/*


FROM ${BASE_DISTRO}:${BASE_VERSION} as yum-base

RUN yum install -y -q \
           wget \
           bzip2 && \
    yum clean packages && \
    rm -rf /var/cache/yum/* /var/tmp/*


FROM ${PACKAGE_MANAGER}-base as install-conda
ARG PYTHON_VERSION

# Install Miniconda
RUN cd /tmp && \
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-$(uname -m).sh && \
    bash Miniconda3-latest-Linux-$(uname -m).sh -b -p /opt/conda && \
    /opt/conda/bin/conda install python==${PYTHON_VERSION} && \
    /opt/conda/bin/conda clean -yaf
RUN echo -e "{\
    \n  \"build_version\": \"$PYTHON_VERSION\" \
    \n}" > /opt/conda/manifest.json


FROM ${BASE_DISTRO}:${BASE_VERSION} as final
LABEL maintainer=blake.dewey@jhu.edu
# Copy Build Artifacts
COPY --from=install-conda /opt/conda /opt/conda

# Update Environment Variables
ENV PATH /opt/conda/bin:${PATH}

CMD ["/bin/bash"]
